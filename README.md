# Bitcoin project

![Quarkus](https://design.jboss.org/quarkus/logo/final/PNG/quarkus_logo_horizontal_rgb_200px_default.png)
Projeto de estudo do framework Quarkus. Tecnologias Maven, Panache, JPA, Hibernate, Java 11.
This project uses Quarkus, the Supersonic Subatomic Java Framework.
If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

### Executando a aplicação em modo de desenvolvimento
```shell script
mvn package quarkus:dev
```
You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```
### AOT
Um dos motivos do Quarkus ser muito rápido em tempo de inicialização, é que ele utiliza uma técnica chamada Ahead-of-time (AOT) e isso significa que o código será compilado antecipadamente. Essa técnica irá ajudar a eliminar sobrecargas de compilação durante a execução da aplicação. Isto é feito transformando o bytecode da VM (Virtual Machine) em código de máquina. Em aplicações padrões, o que ocorre é que o bytecode será executado e frequentemente o código será compilado para linguagem de máquina, usando o Just-in-Time (JIT).

### Nativo
Se usarmos o https://code.quarkus.io/ para criar a nossa aplicação, será fornecido o profile necessário para geração da imagem nativa no pom.xml.

```xml
<profiles>
    <profile>
        <id>native</id>
        <properties>
            <quarkus.package.type>native</quarkus.package.type>
        </properties>
    </profile>
</profiles>
```
Se não utilizar o profile, será necessário informar a propriedade `Dquarkus.package.type=native` na linha de comando, porém é interessante o profile, porquê ele permitirá que testes de imagem nativa sejam executados.

### Teste de integração
É necessário fazer uma requisição real para a API.
Testes de integração têm a característica de, realmente, fazer a integração com o recurso a ser testado. Pode ser uma API, como no caso da aplicação do curso, como um banco de dados, fila, entre outros.

#### Teste Unitário
`Mockito` não consegue mockar direto um Active Records, dessa forma Panache Mock foi utilizado para testar active records. O mockito não consegue mockar os métodos estáticos de PanacheEntityBase, sendo necessário usar o PanacheMock.mock(..).
`Assertions` é uma coleção de métodos utilitários que suportam a afirmação de condições em testes.
Utiliza Mockito para testar Repositorys.

### Plugin responsável pelo live-coding
```
<plugins>
    <plugin>
        <groupId>io.quarkus</groupId>
        <artifactId>quarkus-maven-plugin</artifactId>
        <version>${quarkus-plugin.version}</version>
        <executions>
            <execution>
                <goals>
                    <goal>generate-code</goal>
                    <goal>generate-code-tests</goal>
                    <goal>build</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
</plugins>
```

### Injeção de Dependências e contextos
Em linhas gerais, o escopo de um bean determina o ciclo de vida de suas instâncias, ou seja, quando e onde uma instância deve ser criada e destruída. Os escopos permitidos pelo Quarkus são:

`javax.enterprise.context.ApplicationScoped`: Uma única instância do bean é criada e compartilhada por todos os pontos de injeção. A instância é criada de forma “preguiçosa” (lazily), ou seja, quando um método é invocado através de um client proxy.

`Client proxy`: Um proxy de cliente é basicamente um objeto que delega todas as invocações de método para uma instância de bean de destino. É uma construção de container que implementa io.quarkus.arc.ClientProxy e estende a classe de bean. Os proxies de cliente apenas delegam invocações de método.

`javax.inject.Singleton`: Mesma ideia do @ApplicationScoped, porém aqui o client proxy não é utilizado, a instância é criada quando um ponto de injeção que resolve para um bean @Singleton está sendo injetado.

`javax.enterprise.context.RequestScope`: A instância é associada à requisição, ou seja, uma instância para cada requisição.

`javax.enterprise.context.Dependent`: O ciclo de vida do @Dependent é limitado ao bean que o injeta. Ele será criado e destruído com o bean que o injeta.


<hr />

### MySQL
Criação do banco de dados e tabelas:

```sql
CREATE DATABASE bitcoin;
SHOW DATABASES;
USE bitcoin;
CREATE TABLE `Usuario` (
	`id` int NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(50) NOT NULL,
	`cpf` VARCHAR(14) NOT NULL,
	`username` VARCHAR(50) NOT NULL,
	`password` VARCHAR(100) NOT NULL,
	PRIMARY KEY(`id`)) ENGINE=InnoDB;
	
CREATE TABLE `Ordem` (
	`id` int NOT NULL AUTO_INCREMENT,
	`preco` decimal(6,2) NOT NULL,
	`tipo` VARCHAR(20) NOT NULL,
	`data` datetime NOT NULL,
	`status` VARCHAR(30) NOT NULL,
	`user_id` int NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `Usuario` (`id`)) ENGINE=InnoDB;

```
Conexão Dbeaver: `BD_URL = "jdbc:mysql://localhost:port/bd_name?useTimezone=true&serverTimezone=UTC"`

#### Entities e Active Record
Usando a extensão Hibernate Panache no pom.xml e estendendo da classe abstrata `PanacheEntityBase` podemos implementar o padrão Active Record.
Active Record é um padrão de projeto que trabalha com a técnica ORM (Object Relational Mapper). O modelo consiste em utilizar as entidades (classe java que representa o objeto do mundo real no java) para chamar os métodos de persistência de dados.

#### Repository
Implementando a classe PanachesRepository podemos utilizar esse padrão.
Repository é um padrão de projeto que trabalha com a técnica ORM (Object Relational Mapper). O modelo consiste em fazer uma abstração do JPA (Java Persistence API). O repository é uma classe que fornece métodos de persistência e geralmente é injetado em uma classe Controller ou Service.

### Segurança
#### @UserDefinition
Essa anotação indica que a entidade anotada é a que o framework irá utilizar para validar as informações do usuário.
Quando um usuário informar as credenciais de acesso para acessar um recurso do sistema, o framework usa a entidade para recuperar as informações do banco de dados e verifica se as credenciais informadas são as mesmas que estão salvas no banco.

### Containerizando a aplicação
Em application.properties altere a url do banco de dados: `quarkus.datasource.jdbc.url=${QUARKUS_DATASOURCE_URL}`. Para conexão com banco de dados dentro do container.
Execute `mvn clean instal -DskipTests` no projeto. O arquivo docker-compose.yml possui configurações para comunicação entre o container do projeto Quarkus e do banco de dados Mysql. Em seguida execute `docker-compose up`.

#### Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `bitcoin-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/bitcoin-1.0.0-SNAPSHOT-runner.jar`.

#### Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/bitcoin-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html.

#### RESTEasy JAX-RS
Guide: https://quarkus.io/guides/rest-json

### Developer
[Kamila Serpa](https://kamilaserpa.github.io/)