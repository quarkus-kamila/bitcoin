package br.com.kamila.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import br.com.kamila.model.Bitcoin;

@Path("/bitcoins")
@RegisterRestClient(configKey = "bitcoin-api")
public interface BitcoinService {

	/*
	 * Acessa url em application properties "bitcoin-api" através da configKey
	 * bitcoin-api/mp-rest/url=https://alura-bitcoin.herokuapp.com
	 */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Bitcoin> listar();
    
}