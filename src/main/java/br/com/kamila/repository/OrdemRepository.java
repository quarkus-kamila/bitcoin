package br.com.kamila.repository;

import javax.enterprise.context.ApplicationScoped;

import br.com.kamila.model.Ordem;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped // Classe terá uma instância criada para o sistema, não para cada requisição
public class OrdemRepository implements PanacheRepository<Ordem> {

}
