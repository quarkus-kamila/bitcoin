package br.com.kamila.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.kamila.enums.Status;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Ordem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Setter
	@Getter
	private Double preco;

	@Setter
	@Getter
	private String tipo;

	@Setter
	@Getter
	private LocalDate data;

	@Setter
	@Getter
	@Enumerated(EnumType.STRING)
	private Status status;

	@Setter
	@Getter
	@Column(name = "user_id")
	private Long userId;

}
