package br.com.kamila.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.kamila.enums.Role;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.security.jpa.Password;
import io.quarkus.security.jpa.Roles;
import io.quarkus.security.jpa.UserDefinition;
import io.quarkus.security.jpa.Username;
import lombok.Getter;
import lombok.Setter;

@Entity
@UserDefinition
public class Usuario extends PanacheEntityBase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	private Long id;

	@Setter
	@Getter
	private String nome;

	@Setter
	@Getter
	private String cpf;

	@Setter
	@Getter
	@Username
	private String username;

	@Password
	private String password;

	@Setter
	@Getter
	@Roles
	private String role;

	public static void inserir(Usuario usuario) {
		usuario.password = BcryptUtil.bcryptHash(usuario.password);
		usuario.role = getRoleByUsername(usuario.username);
		usuario.persist();
	}

	private static String getRoleByUsername(String username) {
		if (username.equals("administrador")) {
			return Role.Types.ADMIN;
		}
		return Role.Types.USER;
	}

	// Ao buscar usuário não retorna o password. Getter mantido para autenticação
	@JsonbTransient
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
