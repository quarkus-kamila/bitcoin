package br.com.kamila.enums;

public enum Role {

	ADMIN(Types.ADMIN), USER(Types.USER);

	public class Types {
		public static final String ADMIN = "admin";
		public static final String USER = "user";
	}

	private final String label;

	private Role(String label) {
		this.label = label;
	}

	public String toString() {
		return this.label;
	}
	
}
